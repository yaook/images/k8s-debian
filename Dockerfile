FROM debian:12.9

RUN set -eux ; \
  DEBIAN_FRONTEND=noninteractive apt-get update ; \
  apt-get install -y --no-install-recommends curl iputils-ping ; \
  rm -rf /var/lib/apt/lists/*
